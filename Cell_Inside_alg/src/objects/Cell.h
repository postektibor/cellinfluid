//
// Created by Tibor Postek on 16/11/2018.
//

#ifndef CELL_INSIDE_ALG_CELL_H
#define CELL_INSIDE_ALG_CELL_H

#include <vector>
#include "Triangle.h"
using namespace std;

class Cell {
private:
    vector<Triangle> *t_list;
public:
    Cell(vector<Triangle> *triangles);
    //copy konstruktor
    Cell(const Cell &source);
    //move kontruktor
    Cell(Cell &&source);

    vector<Triangle> &getAllTriangles();
};


#endif //CELL_INSIDE_ALG_CELL_H
