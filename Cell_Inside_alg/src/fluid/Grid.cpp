//
// Created by Tibor Postek on 16/11/2018.
//

#include "Grid.h"


Grid::Grid(int size_x, int size_y, int size_z, vector<Cell> cells)
        : size_x{size_x}, size_y{size_y}, size_z{size_z}, cells{cells} {
    initDataStructure();
}

void Grid::initDataStructure() {
    data_structure = new Node **[size_x];
    for (size_t x = 0; x < size_x; ++x) {
        *(data_structure + x) = new Node *[size_y];
        for (int y = 0; y < size_y; ++y) {
            data_structure[x][y] = new Node[size_z];
            for (int z = 0; z < size_z; ++z) {
                get_node(x, y, z) = Node{
                        Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                                 numeric_limits<double>::quiet_NaN()}, outer};
            }
        }
    }
}

Grid::~Grid() {
    delete[] data_structure;
}

void Grid::printMesh() {
    for (size_t y = 0; y < size_z; ++y) {
        for (int z = 0; z < size_z; ++z) {
            for (int x = 0; x < size_x; ++x) {
                cout << get_node(x, y, z).flag;
            }
            cout << " -> Y-" << y << ", Z-" << z << endl;
        }
        cout << endl;
    }
}

void Grid::printMesh(int y) {
    for (int z = 0; z < size_z; ++z) {
        for (int x = 0; x < size_x; ++x) {
            cout << get_node(x, y, z).flag;
        }
        cout << " -> Y-" << y << ", Z-" << z << endl;
    }
    cout << endl;
}

void Grid::findingObjectBoundary(Triangle triangle, int pY, Position normal_vector, double d,
                                 vector<Position> *boundaryPoints) {
    int count_NAN{0};
    //ak pretina danu usecku
    bool cuts_A = false;
    bool cuts_B = false;
    bool cuts_C = false;
    //potrebujem zistit suradnice P1 a P2
    vector<Position> P_points;

    double t_AB = (pY - triangle.getA().y) / (triangle.getB().y - triangle.getA().y);
    if (triangle.getB().y - triangle.getA().y == 0.000000 && (t_AB == 0 || t_AB == 1)) {
        count_NAN++;
    }
    double t_BC = (pY - triangle.getB().y) / (triangle.getC().y - triangle.getB().y);
    if (triangle.getC().y - triangle.getB().y == 0.000000 && (t_BC == 0 || t_BC == 1)) {
        count_NAN++;
    }
    double t_AC = (pY - triangle.getA().y) / (triangle.getC().y - triangle.getA().y);
    if (triangle.getC().y - triangle.getA().y == 0.000000 && (t_AC == 0 || t_AC == 1)) {
        count_NAN++;
    }

    switch (count_NAN) {
        case 1:
            //ak count_NAN==1 tak celá jedna hrana trojuholnika pretina tuto rovinu
            if (t_AB == 0) {
                P_points.push_back(triangle.getA());
            } else if (t_AB == 1) {
                P_points.push_back(triangle.getB());
            }
            if (t_BC == 0) {
                P_points.push_back(triangle.getB());
            } else if (t_BC == 1) {
                P_points.push_back(triangle.getC());
            }
            if (t_AC == 0) {
                P_points.push_back(triangle.getA());
            } else if (t_AC == 1) {
                P_points.push_back(triangle.getC());
            }
            goto Process_Boundary_points;
        case 2:
            cerr << "Error, because 2 edges cannot lie on that plane" << endl;
            break;
        case 3:
            //ak count_NAN==3 tak cely trojuholnik pretina tuto rovinu
            P_points.push_back(triangle.getA());
            P_points.push_back(triangle.getB());
            P_points.push_back(triangle.getC());
            break;
        default:
            break;
    };

    //ak je priesecnik cez nejaku hranu
    if (t_AB >= 0 && t_AB <= 1) {
        double pZ = triangle.getA().z + t_AB * (triangle.getB().z - triangle.getA().z);
        double pX = triangle.getA().x_folded + t_AB * (triangle.getB().x_folded - triangle.getA().x_folded);
        if (t_AB == 1) {
            cuts_B = true;
        } else if (t_AB == 0) {
            cuts_A = true;
        }
        P_points.push_back(Position{pX, (double) pY, pZ});
    }
    if (t_BC >= 0 && t_BC <= 1) {
        double pZ = triangle.getB().z + t_BC * (triangle.getC().z - triangle.getB().z);
        double pX = triangle.getB().x_folded + t_BC * (triangle.getC().x_folded - triangle.getB().x_folded);
        if (t_BC == 1) {
            cuts_C = true;
            P_points.push_back(Position{pX, (double) pY, pZ});
            goto Intersection_of_AC;
        } else if (t_BC == 0 && !cuts_B) {
            cuts_B = true;
            P_points.push_back(Position{pX, (double) pY, pZ});
            goto Intersection_of_AC;
        }
        P_points.push_back(Position{pX, (double) pY, pZ});
    }

    Intersection_of_AC:
    if (t_AC >= 0 && t_AC <= 1) {
        double pZ = triangle.getA().z + t_AC * (triangle.getC().z - triangle.getA().z);
        double pX = triangle.getA().x_folded + t_AC * (triangle.getC().x_folded - triangle.getA().x_folded);
        if (t_AC == 1) {
            if (!cuts_C) {
                cuts_C = true;
                P_points.push_back(Position{pX, (double) pY, pZ});
            }
            goto Process_Boundary_points;
        } else if (t_AC == 0) {
            if (!cuts_A) {
                cuts_A = true;
                P_points.push_back(Position{pX, (double) pY, pZ});
            }
            goto Process_Boundary_points;
        }
        P_points.push_back(Position{pX, (double) pY, pZ});
    }

    //Rounding Z-coordinate and compute X-coordinate
    Process_Boundary_points:
    double z_min, z_max;

    switch (P_points.size()) {
        case 2:
            //priesecnikom su 2 body
            if (P_points.at(0).z > P_points.at(1).z) {
                z_max = P_points.at(0).z;
                z_min = P_points.at(1).z;
            } else {
                z_min = P_points.at(0).z;
                z_max = P_points.at(1).z;
            }
            //ak existuje celocislene Z
            if (floor(z_max) - ceil(z_min) >= 0) {
                int z_hlp = ceil(z_min);
                while (z_hlp <= floor(z_max)) {
                    //moze tu byt delenie nulou!!!!!
                    double x = -((normal_vector.y * pY) + (normal_vector.z * z_hlp) + d) / normal_vector.x_folded;
                    boundaryPoints->push_back(Position{x, (double) pY, (double) z_hlp});
                    z_hlp++;
                }
            }
            break;
        case 1:
            //priesecnikom je len jeden bod
            z_max = P_points.at(0).z;
            z_min = P_points.at(0).z;
            //ak existuje celocislene Z
            if (floor(z_max) - ceil(z_min) >= 0) {
                int z_hlp = ceil(z_min);
                while (z_hlp <= floor(z_max)) {
                    //moze tu byt delenie nulou!!!!!
                    double x = -((normal_vector.y * pY) + (normal_vector.z * z_hlp) + d) / normal_vector.x_folded;
                    boundaryPoints->push_back(Position{x, (double) pY, (double) z_hlp});
                    z_hlp++;
                }
            }
            break;
        case 3:
            //priesecnikom su 3 body....ked cely trojuholnik pretina tuto rovinu
            //Zatial to Ignorujem


            cerr << "cely trojuholnik pretina rovinu, to treba doriesit" << endl;


            //usporiadam si tieto body podľa suradnic, ktore prave potrebujem
            /*  sort(P_points.begin(), P_points.end(), compareByPosition_Z);
              double z_max = P_points.at(2).z;
              double z_min = P_points.at(0).z;

              //ak existuje celocislene Z
              if (floor(z_max) - ceil(z_min) >= 0) {
                  //prejdem celociselne body a najdem k nim X z rovnice roviny
                  int z_hlp = ceil(z_min);
                  while (z_hlp <= floor(z_max)) {
                      //skalarny vektor a moj hladany bod mi v rovnici roviny musi dat 0 => viem vypocitat bod x_folded
                      //musim zistit vsetky x_folded, ktore su v danej pY a z_hlp suradnici
                      //takze tu najdem svoje x_folded, ktore mozem odrazu aj nasekat na int pozicie
                      double x_min{};
                      double x_max{};
                      int x_hlp = ceil(x_min);
                      while (x_hlp <= floor(x_max)) {

                          x_hlp++;
                      }
                      Z_points.push_back(Position{x_folded, (double) pY, (double) z_hlp});
                      z_hlp++;
                  }
              }*/
            break;
        default:
            cerr << "For pY=" << pY << " not found the intersection, but it should! " << P_points.size() << endl;
            break;
    }
}

void Grid::markingObjectBoundary(vector<Position> &boundary_points, Position normal_vector) {
    for (size_t k = 0; k < boundary_points.size(); ++k) {
        Position Z_point = boundary_points.at(k);
        int x_low = floor(Z_point.x_folded);
        int x_high = ceil(Z_point.x_folded);

        if (x_high == this->size_x) {
            x_high = 0;
        }

        Position low_vector{x_low - Z_point.x_folded, 0, 0};
        Position high_vector{x_high - Z_point.x_folded, 0, 0};

        double citatel_low{normal_vector.x_folded * low_vector.x_folded + normal_vector.y * low_vector.y +
                           normal_vector.z * low_vector.z};

        double citatel_high{normal_vector.x_folded * high_vector.x_folded + normal_vector.y * high_vector.y +
                            normal_vector.z * high_vector.z};

        //len bez absolutnej hodnoty citatela to funguje!!!
        double cos_alpha_low{citatel_low /
                             sqrt(pow(normal_vector.x_folded, 2) + pow(normal_vector.y, 2) + pow(normal_vector.z, 2)) *
                             sqrt(pow(low_vector.x_folded, 2) + pow(low_vector.y, 2) + pow(low_vector.z, 2))};

        //len bez absolutnej hodnoty citatela to funguje!!!
        double cos_alpha_high{citatel_high /
                              sqrt(pow(normal_vector.x_folded, 2) + pow(normal_vector.y, 2) +
                                   pow(normal_vector.z, 2)) *
                              sqrt(pow(high_vector.x_folded, 2) + pow(high_vector.y, 2) + pow(high_vector.z, 2))};
        //nejaky if a potom mam suradnice pre y a z v Z_Points a x_folded su ako x_low alebo x_high a tieto body v meshi oflagujem

        Position Z_node_temp{Z_point};
        Z_node_temp.x_folded = x_low;


        //Do LB nodov si budem ukladat aj Z_point bod, ktory ma preflagoval, aby som vedel rozhodnut, ze ak su
        // napr bunky strasne natesno, tak aby mi to nepreflagovali
        Node actual_node = get_node((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z);

        if (cos_alpha_low >= 0) {
            //  cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
            //       << " je vo vnutri bunky" << endl;
            if (actual_node.flag == Flag::outer &&
                (isnan(actual_node.Z_point.x_folded) || (!isnan(actual_node.Z_point.x_folded) && (
                        (Z_point.x_folded - Z_node_temp.x_folded) <
                        (actual_node.Z_point.x_folded - Z_node_temp.x_folded))))) {
                //ak je X rozdiel mensi, tak bude boundary, ale ak je == tak input/output
                //to by chcelo nejako doriesit
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point, Flag::boundary);
            } else if (actual_node.flag == Flag::boundary) {
                if (actual_node.Z_point.x_folded != Z_point.x_folded) {
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                             Flag::input_output);
                } else {
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                             Flag::boundary);
                }
            } else {
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                         Flag::outer);
            }
        } else {
            //   cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
            //       << " je von z bunky" << endl;
            markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                     Flag::outer);
        }
        if (x_low != x_high) {
            Z_node_temp.x_folded = x_high;
            actual_node = get_node((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z);
            if (cos_alpha_high >= 0) {
                //    cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
                //        << " je vo vnutri bunky" << endl;
                if (actual_node.flag == Flag::outer &&
                    (isnan(actual_node.Z_point.x_folded) || (!isnan(actual_node.Z_point.x_folded) && (
                            (Z_node_temp.x_folded - Z_point.x_folded) <
                            (Z_node_temp.x_folded -
                             actual_node.Z_point.x_folded))))) {
                    //ak je X rozdiel mensi, tak bude boundary, ale ak je == tak input/output
                    //to by chcelo nejako doriesit
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                             Flag::boundary);
                } else if (actual_node.flag == Flag::boundary) {
                    if (actual_node.Z_point.x_folded != Z_point.x_folded) {
                        markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                                 Flag::input_output);
                    } else {
                        markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                                 Flag::boundary);
                    }
                } else {
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                             Flag::outer);
                }
            } else {
                //    cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
                //         << " je von z bunky" << endl;
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                         Flag::outer);
            }
        }
    }
}

void Grid::markingObjectInside(int pY, int minZ, int maxZ, int minX, int maxX) {
    for (int pZ = minZ; pZ <= maxZ; ++pZ) {
        int BN{0};
        for (int pX = minX; pX < maxX; ++pX) {
            Node U = get_node(pX, pY, pZ);
            if (U.flag == Flag::boundary && BN % 2 == 0) {
                markNode(pX, pY, pZ, Position{(double) pX, (double) pY, (double) pZ}, Flag::input);
                BN++;
            } else if (U.flag != Flag::boundary && BN % 2 != 0) {
                markNode(pX, pY, pZ, Position{(double) pX, (double) pY, (double) pZ}, Flag::inner);
            } else if (U.flag == Flag::boundary && BN % 2 != 0) {
                markNode(pX, pY, pZ, Position{(double) pX, (double) pY, (double) pZ}, Flag::output);
                BN++;
            } else if (U.flag == Flag::input_output && BN % 2 == 0) {
                BN += 2;
            }

            /*
             * este vymysliet aky bude not_defined
             * to ale ceknem len ci U.Z_point.x_folded == pX
             * aspon si myslim...teda vlastne asi nie, lebo to musi byt cela plocha trojuholnika, nie len jedna hrana
             */

        }
    }
}

void Grid::remarkingObjectInside(int pY, int minZ, int maxZ, int minX, int maxX) {
    for (int pZ = minZ; pZ <= maxZ; ++pZ) {
        for (int pX = minX; pX < maxX; ++pX) {
            Node U = get_node(pX, pY, pZ);
            if (U.flag != Flag::outer) {
                markNode(pX, pY, pZ, Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                                              numeric_limits<double>::quiet_NaN()}, Flag::inner);
            } else {
                markNode(pX, pY, pZ, Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                                              numeric_limits<double>::quiet_NaN()}, Flag::outer);
            }
        }
    }
}

void Grid::initialAlgorithm() {
    initDataStructure();
    double min_Py{DBL_MAX};
    double max_Py{DBL_MIN};
    double min_Pz{DBL_MAX};
    double max_Pz{DBL_MIN};
    for (size_t c = 0; c < cells.size(); c++) {
        Cell cell = cells.at(c);
        for (size_t i = 0; i < cell.getAllTriangles().size(); ++i) {
            Triangle triangle = cell.getAllTriangles().at(i);
            //     cout<< "A init: x_folded " << triangle.getA().x_folded <<", y "<< triangle.getA().y << ", z "<< triangle.getA().z<<endl;
            //     cout<< "B init: x_folded " << triangle.getB().x_folded <<", y "<< triangle.getB().y << ", z "<< triangle.getB().z<<endl;
            //      cout<< "C init: x_folded " << triangle.getC().x_folded <<", y "<< triangle.getC().y << ", z "<< triangle.getC().z<<endl;
            Position AB{triangle.getB().x_folded - triangle.getA().x_folded, triangle.getB().y - triangle.getA().y,
                        triangle.getB().z - triangle.getA().z};
            Position AC{triangle.getC().x_folded - triangle.getA().x_folded, triangle.getC().y - triangle.getA().y,
                        triangle.getC().z - triangle.getA().z};
            //vektorovy sucin AB x_folded AC
            Position normal_vector{AB.y * AC.z - AB.z * AC.y, AB.z * AC.x_folded - AB.x_folded * AC.z,
                                   AB.x_folded * AC.y - AB.y * AC.x_folded};
            //      cout<< "NV init: x_folded " << normal_vector.x_folded <<", y "<< normal_vector.y << ", z "<< normal_vector.z<<endl;
            Position normal_vector_reverse{AB.z * AC.y - AB.y * AC.z, AB.x_folded * AC.z - AB.z * AC.x_folded,
                                           AB.y * AC.x_folded - AB.x_folded * AC.y};
            //parameter d z rovnice roviny
            double d = -(normal_vector.x_folded * triangle.getA().x_folded + normal_vector.y * triangle.getA().y +
                         normal_vector.z * triangle.getA().z);

            int minY = ceil(min({triangle.getA().y, triangle.getB().y, triangle.getC().y}));
            int maxY = floor(max({triangle.getA().y, triangle.getB().y, triangle.getC().y}));

            //pre vsetky Py z toho intervalu potrebujem najst vhodne t
            vector<Position> boundaryPoints;

            for (int pY = minY; pY <= maxY; ++pY) {
                findingObjectBoundary(triangle, pY, normal_vector, d, &boundaryPoints);
                markingObjectBoundary(boundaryPoints, normal_vector);
                boundaryPoints.clear();
            }

            check_min_max_x_y(min_Py, max_Py, min_Pz, max_Pz, triangle);
        }
    }
    for (int pY = min_Py; pY < max_Py; ++pY) {
        markingObjectInside(pY, min_Pz, max_Pz, 0, size_x);
    }
    for (int pY = min_Py; pY < max_Py; ++pY) {
        remarkingObjectInside(pY, min_Pz, max_Pz, 0, size_x);
    }
}


void Grid::updateAlgorithm() {
    for (size_t c = 0; c < cells.size(); c++) {
        Cell cell = cells.at(c);

        for (size_t i = 0; i < cell.getAllTriangles().size(); ++i) {
            Triangle triangle = cell.getAllTriangles().at(i);
            //      cout<< "A update: x_folded " << triangle.getA().x_folded <<", y "<< triangle.getA().y << ", z "<< triangle.getA().z<<endl;
            //       cout<< "B update: x_folded " << triangle.getB().x_folded <<", y "<< triangle.getB().y << ", z "<< triangle.getB().z<<endl;
            //       cout<< "C update: x_folded " << triangle.getC().x_folded <<", y "<< triangle.getC().y << ", z "<< triangle.getC().z<<endl;
            Position AB{triangle.getB().x_folded - triangle.getA().x_folded, triangle.getB().y - triangle.getA().y,
                        triangle.getB().z - triangle.getA().z};
            Position AC{triangle.getC().x_folded - triangle.getA().x_folded, triangle.getC().y - triangle.getA().y,
                        triangle.getC().z - triangle.getA().z};
            //vektorovy sucin AB x_folded AC
            Position normal_vector{AB.y * AC.z - AB.z * AC.y, AB.z * AC.x_unfolded - AB.x_unfolded * AC.z,
                                   AB.x_unfolded * AC.y - AB.y * AC.x_unfolded};
            //     cout<< "NV update: x_folded " << normal_vector.x_folded <<", y "<< normal_vector.y << ", z "<< normal_vector.z<<endl;
            //parameter d z rovnice roviny
            double d = -(normal_vector.x_folded * triangle.getA().x_folded + normal_vector.y * triangle.getA().y +
                         normal_vector.z * triangle.getA().z);

            int minY = ceil(min({triangle.getA().y, triangle.getB().y, triangle.getC().y}));
            int maxY = floor(max({triangle.getA().y, triangle.getB().y, triangle.getC().y}));

            //pre vsetky Py z toho intervalu potrebujem najst vhodne t
            vector<Position> boundaryPoints;
            for (int pY = minY; pY <= maxY; ++pY) {
                findingObjectBoundary(triangle, pY, normal_vector, d, &boundaryPoints);
                markingObjectBoundaryUpdate(boundaryPoints, normal_vector);
                boundaryPoints.clear();
            }
        }
    }
}


void Grid::markingObjectBoundaryUpdate(vector<Position> &boundary_points, Position normal_vector) {
    for (size_t k = 0; k < boundary_points.size(); ++k) {
        Position Z_point = boundary_points.at(k);
        int x_low = floor(Z_point.x_folded);
        int x_high = ceil(Z_point.x_folded);

        if (x_high == this->size_x) {
            x_high = 0;
        }


        Position low_vector{x_low - Z_point.x_folded, 0, 0};
        Position high_vector{x_high - Z_point.x_folded, 0, 0};

        double citatel_low{normal_vector.x_folded * low_vector.x_folded + normal_vector.y * low_vector.y +
                           normal_vector.z * low_vector.z};

        double citatel_high{normal_vector.x_folded * high_vector.x_folded + normal_vector.y * high_vector.y +
                            normal_vector.z * high_vector.z};

        //len bez absolutnej hodnoty citatela to funguje!!!
        double cos_alpha_low{citatel_low /
                             sqrt(pow(normal_vector.x_folded, 2) + pow(normal_vector.y, 2) + pow(normal_vector.z, 2)) *
                             sqrt(pow(low_vector.x_folded, 2) + pow(low_vector.y, 2) + pow(low_vector.z, 2))};

        //len bez absolutnej hodnoty citatela to funguje!!!
        double cos_alpha_high{citatel_high /
                              sqrt(pow(normal_vector.x_folded, 2) + pow(normal_vector.y, 2) +
                                   pow(normal_vector.z, 2)) *
                              sqrt(pow(high_vector.x_folded, 2) + pow(high_vector.y, 2) + pow(high_vector.z, 2))};
        //nejaky if a potom mam suradnice pre y a z v Z_Points a x_folded su ako x_low alebo x_high a tieto body v meshi oflagujem

        Position Z_node_temp{Z_point};
        Z_node_temp.x_folded = x_low;


        //Do LB nodov si budem ukladat aj Z_point bod, ktory ma preflagoval, aby som vedel rozhodnut, ze ak su
        // napr bunky strasne natesno, tak aby mi to nepreflagovali
        Node actual_node = get_node((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z);

        if (cos_alpha_low >= 0) {
            //  cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
            //       << " je vo vnutri bunky" << endl;
            if ((isnan(actual_node.Z_point.x_unfolded) || (!isnan(actual_node.Z_point.x_unfolded) && (
                    (Z_point.x_unfolded - Z_node_temp.x_folded) <=
                    (actual_node.Z_point.x_unfolded - Z_node_temp.x_folded))))) {
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point, Flag::inner);
            } else if (actual_node.flag == Flag::inner) {
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                         Flag::inner);
            } else {
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z,
                         Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                                  numeric_limits<double>::quiet_NaN()},
                         Flag::outer);
            }
        } else {
            //   cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
            //       << " je von z bunky" << endl;
            markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z,
                     Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                              numeric_limits<double>::quiet_NaN()},
                     Flag::outer);
        }
        if (x_low != x_high) {
            Z_node_temp.x_folded = x_high;
            actual_node = get_node((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z);
            if (cos_alpha_high >= 0) {
                //    cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
                //        << " je vo vnutri bunky" << endl;
                if ((isnan(actual_node.Z_point.x_unfolded) || (!isnan(actual_node.Z_point.x_unfolded) && (
                        (Z_node_temp.x_folded - Z_point.x_unfolded) <=
                        (Z_node_temp.x_folded - actual_node.Z_point.x_unfolded))))) {
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                             Flag::inner);
                } else if (actual_node.flag == Flag::inner) {
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z, Z_point,
                             Flag::inner);
                } else {
                    markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z,
                             Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                                      numeric_limits<double>::quiet_NaN()},
                             Flag::outer);
                }
            } else {
                //    cout << "Bod " << "x_folded= " << Z_node_temp.x_folded << ", y= " << Z_node_temp.y << " z= " << Z_node_temp.z
                //         << " je von z bunky" << endl;
                markNode((int) Z_node_temp.x_folded, (int) Z_node_temp.y, (int) Z_node_temp.z,
                         Position{numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN(),
                                  numeric_limits<double>::quiet_NaN()},
                         Flag::outer);
            }
        }
    }
}


void Grid::check_min_max_x_y(double &min_y, double &max_y, double &min_z, double &max_z, Triangle triangle) {
    if (triangle.getA().y < min_y) {
        min_y = triangle.getA().y;
    }
    if (triangle.getB().y < min_y) {
        min_y = triangle.getB().y;
    }
    if (triangle.getC().y < min_y) {
        min_y = triangle.getC().y;
    }
    if (triangle.getA().y > max_y) {
        max_y = triangle.getA().y;
    }
    if (triangle.getB().y > max_y) {
        max_y = triangle.getB().y;
    }
    if (triangle.getC().y > max_y) {
        max_y = triangle.getC().y;
    }

    if (triangle.getA().y < min_z) {
        min_z = triangle.getA().y;
    }
    if (triangle.getB().y < min_z) {
        min_z = triangle.getB().y;
    }
    if (triangle.getC().y < min_z) {
        min_z = triangle.getC().y;
    }
    if (triangle.getA().y > max_z) {
        max_z = triangle.getA().y;
    }
    if (triangle.getB().y > max_z) {
        max_z = triangle.getB().y;
    }
    if (triangle.getC().y > max_z) {
        max_z = triangle.getC().y;
    }
}

void Grid::markNode(int x, int y, int z, Position Z_point, Flag flag) {
    get_node(x, y, z) = Node{Z_point, flag};
}

void Grid::moveCells(double stepX) {
    for (size_t c = 0; c < cells.size(); c++) {
        Cell cell = cells.at(c);
        for (size_t i = 0; i < cell.getAllTriangles().size(); ++i) {
            Triangle *triangle = &cell.getAllTriangles().at(i);
            triangle->move_all_x_positions(stepX, this->size_x);
        }
    }
}

Node &Grid::get_node(int x, int y, int z) {
    return data_structure[x][y][z];
}
