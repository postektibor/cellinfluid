//
// Created by Tibor Postek on 16/11/2018.
//

#include "Triangle.h"


Triangle::Triangle(Position A, Position B, Position C)
        : A{A}, B{B}, C{C} {
}

Position Triangle::getA() {
    return this->A;
}

Position Triangle::getB() {
    return this->B;
}

Position Triangle::getC() {
    return this->C;
}

void Triangle::move_all_x_positions(double stepX, double max_value_of_x) {
    if (A.x_folded + stepX >= max_value_of_x) {
        A.x_folded = fmod((A.x_folded + stepX), max_value_of_x);
    } else {
        A.x_folded += stepX;
    }

    if (B.x_folded + stepX >= max_value_of_x) {
        B.x_folded = fmod((B.x_folded + stepX), max_value_of_x);
    } else {
        B.x_folded += stepX;
    }

    if (C.x_folded + stepX >= max_value_of_x) {
        C.x_folded = fmod((C.x_folded + stepX), max_value_of_x);
    } else {
        C.x_folded += stepX;
    }
    A.x_unfolded += stepX;
    B.x_unfolded += stepX;
    C.x_unfolded += stepX;
}
