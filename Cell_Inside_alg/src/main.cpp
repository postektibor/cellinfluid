#include <iostream>
#include "fluid/Grid.h"
#include <string>
#include <fstream>
#include <sstream>

using namespace std;


vector<Triangle> *
load_triangles_from_file(string filename_nodes, string filename_triangles, int count_of_triangles, double position_x,
                         double position_y, double position_z, double scaling) {
    vector<Triangle> *vect = new vector<Triangle>;
    double nodes[count_of_triangles][3];
    double x{0}, y{0}, z{0};
    ifstream file_nodes, file_triangles;
    file_nodes.open(filename_nodes);
    if (!file_nodes) {
        std::cerr << "Problem with opening file " << filename_nodes << std::endl;
        return vect;
    }
    string line;
    int line_number{0};
    while (getline(file_nodes, line)) {
        istringstream iss(line);
        if (!(iss >> x >> y >> z)) {
            cerr << "Error with line of file" << endl;
            break;
        }
        nodes[line_number][0] = (position_x + (x * scaling));
        nodes[line_number][1] = (position_y + (y * scaling));
        nodes[line_number][2] = (position_z + (z * scaling));
        line_number++;
    }
    file_nodes.close();

    file_triangles.open(filename_triangles);
    if (!file_triangles) {
        std::cerr << "Problem with opening file " << filename_triangles << std::endl;
        return vect;
    }
    line = "";
    int A{0}, B{0}, C{0};
    while (getline(file_triangles, line)) {
        istringstream iss(line);
        if (!(iss >> A >> B >> C)) {
            cerr << "Error with line of file" << endl;
            break;
        }
        Triangle triangle{Position{nodes[A][0], nodes[A][1], nodes[A][2]},
                          Position{nodes[B][0], nodes[B][1], nodes[B][2]},
                          Position{nodes[C][0], nodes[C][1], nodes[C][2]}};
        vect->push_back(triangle);
    }
    file_triangles.close();

    return vect;
}

int main() {

/*    Position A{0.5, 1.3, 1.72};
    Position B{3.38, 2.39, 0.17};
    Position C{0.98, 5.7, 0.83};
    Position D{6.32, 6.28, 0.82};
    Position E{7.320, 0.67, 1.28};
    Position F{3.18, 0.37, 2};
    Position G{3.81, 3.53, 4.59};


//pozor, lebo tak ako si ich ulozim tie vrcholy, od toho sa mi odvija pravidlo pravej ruky!
    Triangle t1(A, B, C);
    Triangle t2(C, B, D);
    Triangle t3(D, B, E);
    Triangle t4(D, B, E);
    Triangle t5(E, B, F);
    Triangle t6(F, B, A);

    Triangle t7(D, E, G);
    Triangle t8(C, D, G);
    Triangle t9(A, C, G);
    Triangle t10(F, A, G);
    Triangle t11(E, F, G);

    vector<Triangle> vector1{t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11};

    Cell cell1{&vector1};

    Position A2{20.5, 1.3, 5.72};
    Position B2{23.38, 2.39, 4.17};
    Position C2{20.98, 5.7, 4.83};
    Position D2{26.32, 6.28, 4.82};
    Position E2{27.320, 0.67, 5.28};
    Position F2{23.18, 0.37, 6};
    Position G2{23.81, 3.53, 9.59};


//pozor, lebo tak ako si ich ulozim tie vrcholy, od toho sa mi odvija pravidlo pravej ruky!
    Triangle t12(A2, B2, C2);
    Triangle t22(C2, B2, D2);
    Triangle t32(D2, B2, E2);
    Triangle t42(D2, B2, E2);
    Triangle t52(E2, B2, F2);
    Triangle t62(F2, B2, A2);

    Triangle t72(D2, E2, G2);
    Triangle t82(C2, D2, G2);
    Triangle t92(A2, C2, G2);
    Triangle t102(F2, A2, G2);
    Triangle t112(E2, F2, G2);*/

    //  vector<Triangle> loaded_triangles = *load_triangles_from_file("../tetrahedron_nodes.dat",
//                                                                  "../tetrahedron_triangles.dat", 4, 10, 10, 19, 8, 1, 1, 1);

    vector<Triangle> loaded_triangles2 = *load_triangles_from_file("../4sten_nodes.dat",
                                                                   "../4sten_triangles.dat", 4, 5, 5, 5, 1);

//    vector<Triangle> vector2{t12, t22, t32, t42, t52, t62, t72, t82, t92, t102, t112};

    //  Cell cell{&loaded_triangles};
    Cell cell2{&loaded_triangles2};

    vector<Cell> cells{cell2};

    Grid grid(10, 10, 10, cells);


    grid.initialAlgorithm();
    grid.printMesh();

    /* for (int i = 0; i < 80; ++i) {
         grid.moveCells(0.5);
         grid.updateAlgorithm();
         grid.printMesh(11);
     }*/

    return 0;
}
