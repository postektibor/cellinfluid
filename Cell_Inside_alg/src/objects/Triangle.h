//
// Created by Tibor Postek on 16/11/2018.
//

#ifndef CELL_INSIDE_ALG_TRIANGLE_H
#define CELL_INSIDE_ALG_TRIANGLE_H
#include <ostream>
#include "Position.h"
#include <cmath>

class Triangle {
private:
    Position A, B, C;
public:
    Triangle(Position A, Position B, Position C);

    Position getA();
    Position getB();
    Position getC();

    void move_all_x_positions(double stepX, double max_value_of_x);

};


#endif //CELL_INSIDE_ALG_TRIANGLE_H
