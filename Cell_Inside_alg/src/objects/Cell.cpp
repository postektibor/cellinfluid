//
// Created by Tibor Postek on 16/11/2018.
//

#include "Cell.h"

Cell::Cell(vector<Triangle> *triangles) : t_list{triangles}{
}

Cell::Cell(const Cell &source) :Cell{source.t_list}{}

Cell::Cell(Cell &&source) : t_list{source.t_list}{
    source.t_list = nullptr;
}

vector<Triangle>& Cell::getAllTriangles() {
    return *t_list;
}