//
// Created by Tibor Postek on 29/01/2019.
//

#ifndef CELL_INSIDE_ALG_POSITION_H
#define CELL_INSIDE_ALG_POSITION_H
struct Position {
    double x_folded, y, z, x_unfolded=0;
    // friend std::ostream& operator<< (std::ostream&, const Position&);
};
/*
std::ostream& operator<< (std::ostream& myOut, const Position& myPosition){
    myOut << "x_folded= " << myPosition.x_folded <<", y= " << myPosition.y << " z= " << myPosition.z;
    return myOut;
}*/
#endif //CELL_INSIDE_ALG_POSITION_H
