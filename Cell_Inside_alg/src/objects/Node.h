//
// Created by Tibor Postek on 29/01/2019.
//

#ifndef CELL_INSIDE_ALG_FLAG_H
#define CELL_INSIDE_ALG_FLAG_H

#include "Position.h"

enum Flag {
    outer, boundary, input, inner, output, input_output, not_defined
};

struct Node {
    Position Z_point;
    Flag flag;
};

#endif //CELL_INSIDE_ALG_FLAG_H
