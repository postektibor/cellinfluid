//
// Created by Tibor Postek on 16/11/2018.
//

#ifndef CELL_INSIDE_ALG_MESH_H
#define CELL_INSIDE_ALG_MESH_H

#include "iostream"
#include "../objects/Cell.h"
#include "math.h"
#include <algorithm>
#include "../objects/Node.h"
#include <cfloat>
#include <cmath>

using namespace std;

class Grid {
private:
    Node ***data_structure;
    int size_x;
    int size_y;
    int size_z;
    vector<Cell> cells;
    int coutOfMarkedNodes{0};

    void findingObjectBoundary(Triangle triangle, int pY, Position normal_vector, double d,
                               vector<Position> *boundaryPoints);

    void markingObjectBoundary(vector<Position> &boundary_points, Position normal_vector);

    void markingObjectBoundaryUpdate(vector<Position> &boundary_points, Position normal_vector);

    void markingObjectInside(int pY, int minZ, int maxZ, int minX, int maxX);

    void remarkingObjectInside(int pY, int minZ, int maxZ, int minX, int maxX);

    void initDataStructure();

    void check_min_max_x_y(double &min_y, double &max_y, double &min_z, double &max_z, Triangle triangle);

    Node &get_node(int x, int y, int z);

public:
    Grid(int size_x, int size_y, int size_z, vector<Cell> cells);

    ~Grid();

    void initialAlgorithm();

    void updateAlgorithm();

    void markNode(int x, int y, int z, Position Z_point, Flag flag);

    void printMesh();

    void printMesh(int y);

    static bool compareByPosition_Z(const Position &a, const Position &b) {
        return a.z < b.z;
    };

    void moveCells(double stepX);

};


#endif //CELL_INSIDE_ALG_MESH_H
